<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cuestionario extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'adultos',
        'menores',
        'veganos',
        'intolerancia',
        'autobus',
        'hora',
        'mascota_perro',
        'mascota_gato',
    ];

    public function usuario()
    {
        return $this->belongsTo(User::class);
    }
}
