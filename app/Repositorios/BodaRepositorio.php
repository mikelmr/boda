<?php

namespace App\Repositorios;

use App\Boda;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;

class BodaRepositorio
{
    public static function obtener()
    {
        return Session::get('boda', self::cargar());
    }

    private static function cargar()
    {
        $boda = Boda::first();

        if($boda == null) {
            $boda = self::crearBodaPorDefecto();
        }

        Session::put('boda', $boda);
        return $boda;
    }

    private static function crearBodaPorDefecto()
    {
        $bodaPorDefecto = new Boda();
        $bodaPorDefecto->nombres = 'Nombres';
        $bodaPorDefecto->fecha = new Carbon('2222-02-02');

        return $bodaPorDefecto;
    }
}
