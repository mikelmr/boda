<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Boda extends Model
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'fecha',
    ];

    public function obtenerImagenCabecera()
    {
        $imagenCabecera = Imagen::where('nombre', 'cabecera.jpg')->first();
        if ($imagenCabecera == null) {
            return 'img/cabecera.png';
        }

        return $imagenCabecera->ruta();
    }

    public function obtenerImagenPortada()
    {
        $imagenPortada = Imagen::where('nombre', 'portada.jpg')->first();
        if ($imagenPortada == null) {
            return 'img/portada.png';
        }

        return $imagenPortada->ruta();
    }

    public function obtenerDiasFaltan()
    {
        return Carbon::now()->diffInDays($this->fecha);
    }

    public function obtenerFechaTexto()
    {
        return $this->fecha->day . ' de ' .
            ucfirst($this->fecha->monthName) . ' del ' .
            $this->fecha->year;
    }

    public function haLlegadoFecha()
    {
        return $this->fecha < Carbon::now();
    }
}
