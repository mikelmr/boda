<?php

namespace App\Http\Controllers;

use App\Imagen;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class MisFotosController extends Controller
{
     public function index()
     {
        $fotos = Imagen::whereNull('eliminada')->whereUserId(Auth::user()->id)->get();

        return view('mis-fotos', ['fotos' => $fotos]);
     }

     public function store(Request $request)
     {
      $request->validate([
         'fotos' => 'required',
         'fotos.*' => 'image'
       ],[
          'fotos.required' => 'Creo que se te ha olvidado seleccionar al menos una foto',
          'fotos.*.image' => 'El formato de la foto no es correcto',
       ]);

       if($request->hasfile('fotos')) {
           foreach($request->file('fotos') as $ficheroFoto)
           {
               $foto = new Imagen();
               $foto->nombre = Str::random(60) . '.' . $ficheroFoto->getClientOriginalExtension();
               $foto->user_id = Auth::user()->id;
               $foto->publico = false;

               $ficheroFoto->move(storage_path('imagenes'), $foto->nombre);

               $foto->save();
           }
   
   
          return back()->with('success', 'Todo guardado correctamente!');
       }
     }

     public function destroy (Imagen $foto)
     {
         if ($foto->user_id == Auth::user()->id) {
            $foto->eliminada = Carbon::now();
            $foto->save();
         }

         return back()->with('success', 'Foto eliminada!');;
     }
}
