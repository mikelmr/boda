<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    public function get($password)
    {
        $slug = Str::slug($password);
        $usuario = User::where('password', $slug)->first();
        Auth::login($usuario);

        return redirect('');
    }

    public function destroy()
    {
        Auth::logout();

        return redirect('');
    }
}
