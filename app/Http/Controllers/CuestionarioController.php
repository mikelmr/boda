<?php

namespace App\Http\Controllers;

use App\Cuestionario;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CuestionarioController extends Controller
{
    //

    public function index(int $idUsuario = null)
    {        
        if ($idUsuario) {
            $cuestionario = User::findOrFail($idUsuario)->cuestionario;
        } else {
            $cuestionario = Auth::user()->cuestionario;
        }
        if (!$cuestionario) {
            $cuestionario = new Cuestionario();
        }

        return view('cuestionario', compact('cuestionario', 'idUsuario'));
    }

    public function store(Request $request)
    {
        $formulario = $request->validate([
            'adultos' => 'required|numeric',
            'menores' => 'nullable|numeric',
            'veganos' => 'nullable|numeric',
            'intolerancia' => 'nullable',
            'autobus' => 'required|in:autobus,coche',
            'hora' => 'nullable|in:medianoche,madrugada',
            'mascota_perro' => 'nullable',
            'mascota_gato' => 'nullable',
        ],[
            'adultos.required' => 'No va ningún adulto?',
            'adultos.numeric' => 'El número de adultos tiene que ser un número.',
            'menores.numeric' => 'El número de menores tiene que ser un número.',
            'veganos.numeric' => 'El número de veganos tiene que ser un número.',
            'autobus.required' => 'Que has hecho para no seleccionar el medio de transporte?',
            'autobus.in' => 'Qué opción es esa como medio de transporte?',
            'hora.in' => 'Qué horario has cogido?',
        ]);

        if ($idUsuario = request()->input('idUsuario')) {
            $usuario = User::findOrFail($idUsuario);
        } else  {
            $usuario = Auth::user();
        }

        if (!$usuario->cuestionario) {
            $usuario->cuestionario()->create($formulario);
        } else {
            $usuario->cuestionario->update($formulario);
        }

        return back();
    }
}
