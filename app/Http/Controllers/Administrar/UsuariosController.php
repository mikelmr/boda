<?php

namespace App\Http\Controllers\Administrar;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Database\Eloquent\Collection;

class UsuariosController extends Controller
{
    //

    public function index()
    {        
        $usuarios = User::all();
        return view('administrar/usuarios/index', compact('usuarios'));
    }

    public function reset(int $idUsuario = null)
    {

        if ($idUsuario) {
            $usuarios = new Collection([User::findOrFail($idUsuario)]);
        } else {
            $usuarios = User::all();
        }
        
        foreach ($usuarios as $usuario) {
            $usuario->resetearToken();
        }

        return redirect('/administrar/usuarios');
    }
}
