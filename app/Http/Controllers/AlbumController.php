<?php

namespace App\Http\Controllers;

use App\Imagen;
use App\Repositorios\BodaRepositorio;
use Illuminate\Support\Facades\Auth;

class AlbumController extends Controller
{
    //

    public function index()
    {     
        $boda = BodaRepositorio::obtener();

        if (!$boda->haLlegadoFecha() && !Auth::user()->tienePermiso('Administrador') ) {
            return view('futuro');
        }

        $fotos = Imagen::compartida()
            ->paginate(4);

        return view('album', [
            'fotos' => $fotos,
        ]);
    }

    public function get(Imagen $foto)
    {     
        return view('foto', [
            'foto' => $foto,
            'siguiente' => $foto->siguiente(),
            'anterior' => $foto->anterior(),
        ]);
    }
}
