<?php

namespace App\Http\Controllers;

use App\Evento;

class InformacionController extends Controller
{
    //

    public function index()
    {        
        $eventos = Evento::all()->sortBy('hora');
        return view('informacion', compact('eventos'));
    }
}
