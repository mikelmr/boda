<?php

namespace App\Http\Controllers;

use App\Imagen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class ImagenController extends Controller
{
     public function get(Imagen $imagen)
     {
        if (!$imagen->publico && !Auth::check()) {
           return;
        }
        
        $path = storage_path('imagenes/'.$imagen->nombre);
        $mime = pathinfo($path, PATHINFO_EXTENSION);
        $type = "image/{$mime}";
        header('Content-Type:'.$type);
        header('Content-Length: ' . filesize($path));
        readfile($path);
     }

     public function index()
     {
        $imagenes = Imagen::whereUserId(Auth::user()->id)->get();

        return view('imagenes', ['imagenes' => $imagenes]);
     }

     public function store(Request $request)
     {
      $request->validate([
         'fotos' => 'required',
         'fotos.*' => 'image'
       ],[
          'fotos.required' => 'Creo que se te ha olvidado seleccionar al menos una foto',
          'fotos.*.image' => 'El formato de la foto no es correcto',
       ]);

       if($request->hasfile('fotos')) {
           foreach($request->file('fotos') as $ficheroFoto)
           {
               $foto = new Imagen();
               $foto->nombre = Str::random(60) . '.' . $ficheroFoto->getClientOriginalExtension();
               $foto->user_id = Auth::user()->id;
               $foto->publico = false;

               $ficheroFoto->move(storage_path('imagenes'), $foto->nombre);

               $foto->save();
           }
   
   
          return back()->with('success', 'Todo guardado correctamente!');
       }
     }
}
