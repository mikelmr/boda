<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class InvitacionController extends Controller
{
    //

    public function index()
    {
        $preguntar_asistencia = Auth::user()->asistencia_confirmada === null;
        $solo_imagenes = array_key_exists('solo_imagenes', request()->all());
        return view('invitacion', compact('preguntar_asistencia','solo_imagenes'));
    }

    public function store()
    {
        $asistencia = request()->input('asistencia');

        if (!$asistencia) {
            return redirect('asistencia');
        }

        $usuario = Auth::user();
        $usuario->asistencia_confirmada = ($asistencia == 'si');
        $usuario->save();

        if ($usuario->asistencia_confirmada) {
            return redirect('cuestionario');
        } else {
            return redirect('no-asistir');
        }
    }
}
