<?php

namespace App\Http\Controllers;

class NoAsistirController extends Controller
{
    public function index()
    {        
        return view('no-asistir');
    }
}
