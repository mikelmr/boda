<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AsistenciaMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::user() || Auth::user()->asistencia_confirmada) {
            return $next($request);
        }
        
        if (Auth::user()->asistencia_confirmada === null) {
            return redirect('/invitacion');
        } else {
            return redirect('/no-asistir');
        }
    }
}
