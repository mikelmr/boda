<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'hora',
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'eventos';
}
