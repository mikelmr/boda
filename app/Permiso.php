<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permiso extends Model
{    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
    ];

    public function padre()
    {
        return $this->belongsTo(Permiso::class, 'padre');
    }

    public function hijos()
    {
        return $this->hasMany(Permiso::class, 'padre', 'id');
    }

    public function tienePermiso(string $nombrePermiso) : bool
    {
        if (strpos($nombrePermiso, $this->nombre) === 0 ) {
            return true;
        }

        foreach ($this->hijos as $hijo) {
            if ($hijo->tienePermiso($nombrePermiso)) {
                return true;
            }
        }

        return false;
    }
}
