<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'usuario', 'name', 'password', 'asistencia_confirmada',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'token',
    ];

    public function cuestionario()
    {
        return $this->hasOne(Cuestionario::class);
    }

    public function permisos()
    {
        return $this->belongsToMany(Permiso::class)->with('hijos');
    }

    public function tienePermiso(string $nombrePermiso) : bool
    {
        foreach ($this->permisos as $permiso) {
            if ($permiso->tienePermiso($nombrePermiso)) {
                return true;
            }
        }

        return false;
    }

    public function resetearToken()
    {
        $this->token = Str::slug(Str::random(60));

        $this->save();
    }

    public function obtenerUrlReseteo() : string
    {
        if (empty($this->token)) {
            return '';
        }

        return URL::to('nuevo-' . $this->token);
    }
}
