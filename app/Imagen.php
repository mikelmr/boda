<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imagen extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'imagenes';

    public function getRouteKeyName()
    {
        return 'nombre';
    }

    public function ruta()
    {
        return '/imagenes/' . $this->nombre;
    }

    public function scopeCompartida($query)
    {
        return $query
            ->whereNull('eliminada')
            ->whereNotNull('compartida');
    }

    public function siguiente()
    {
        return Imagen::compartida()->where('id', ">", $this->id)->first();
    }

    public function anterior()
    {
        return Imagen::compartida()->where('id', "<", $this->id)->latest()->first();
    }
}
