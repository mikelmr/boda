module.exports = {
  purge: [
    './resources/views/**/*.blade.php',
    './resources/css/**/*.css',
    './resources/js/components/**/*.vue',
  ],
  theme: {
    extend: {},
    fontFamily: {
      sans: ['Open Sans'],
      serif: ['Open Sans'],
      mono: ['Open Sans'],
      display: ['Open Sans'],
      body: ['Open Sans']
    },
  },
  variants: {},
  plugins: [],
}
