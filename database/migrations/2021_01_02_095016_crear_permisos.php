<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearPermisos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permisos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->unsignedBigInteger('padre')->nullable();
            $table->timestamps();
        });

        Schema::create('permiso_user', function (Blueprint $table) {
            $table->primary(['user_id', 'permiso_id']);

            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('permiso_id');
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('permiso_id')
                ->references('id')
                ->on('permisos')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuestionarios');
        Schema::dropIfExists('cuestionarios');
        Schema::dropIfExists('cuestionarios');
        Schema::dropIfExists('cuestionarios');
    }
}
