<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaCuestionario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cuestionarios', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->unique();
            $table->integer('adultos')->unsigned();
            $table->integer('menores')->unsigned()->nullable();
            $table->integer('veganos')->unsigned()->nullable();
            $table->string('intolerancia')->nullable();
            $table->string('autobus')->nullable();
            $table->string('hora')->nullable();
            $table->string('mascota_perro')->nullable();
            $table->string('mascota_gato')->nullable();
            $table->timestamps();        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cuestionarios');
    }
}
