<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'asistencia'], function() {
    Route::get('/', 'HomeController@index')->name('home');
});

Route::get('/imagenes/{imagen}', 'ImagenController@get');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/no-asistir', 'NoAsistirController@index');
    Route::get('/invitacion', 'InvitacionController@index');
    Route::post('/invitacion', 'InvitacionController@store');
    Route::group(['middleware' => 'asistencia'], function() {
        Route::get('/cuestionario', 'CuestionarioController@index');
        Route::post('/cuestionario', 'CuestionarioController@store');
        Route::get('/informacion', 'InformacionController@index');
        Route::get('/album', 'AlbumController@index');
        Route::get('/album/{foto}', 'AlbumController@get');

        Route::get('/mis-fotos', 'MisFotosController@index');
        Route::post('/mis-fotos', 'MisFotosController@store');
        Route::delete('/mis-fotos/{foto}', 'MisFotosController@destroy');
    });
});

Route::group([ 'prefix' => 'administrar', 'middleware' => 'tienePermiso'], function () {
    Route::get('/usuarios', 'Administrar\UsuariosController@index');
    Route::get('/usuarios/reset', 'Administrar\UsuariosController@reset');
    Route::get('/usuarios/{usuario}/reset', 'Administrar\UsuariosController@reset');
    Route::get('/cuestionario/{usuario}', 'CuestionarioController@index');
});

// Authentication Routes...
Route::get('entrar', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('entrar', 'Auth\LoginController@login');
Route::post('salir', 'Auth\LoginController@logout')->name('logout');

// Password Reset Routes...
Route::get('nuevo-{token}', 'Auth\ResetPasswordController@showResetForm');
Route::post('nuevo', 'Auth\ResetPasswordController@reset')->name('password.update');