<?php

namespace Tests\Feature;

use App\Boda;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class HomeTest extends TestCase
{
    use DatabaseMigrations, WithFaker;

    /** @test */
    public function visitar_home_por_defecto_ok()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
        $response->assertSeeText('Nos casamos');
        $response->assertSeeText('Nombres');
        $response->assertSeeText('Febrero');
        $response->assertSeeText('2222');
        $response->assertSee('img/cabecera.png');
        $response->assertSee('img/portada.png');

        $dias = Carbon::now()->diffInDays(new Carbon('2222-02-02'));

        $response->assertSeeText($dias);
    }

    /** @test */
    public function visitar_home_aleatoria_ok()
    {
        $boda = $this->crearBoda();

        $response = $this->get('/');

        $response->assertStatus(200);
        $response->assertSeeText('Nos casamos');
        $response->assertSeeText($boda->nombres);

        $response->assertSeeText(ucfirst($boda->fecha->monthName));
        $response->assertSeeText($boda->fecha->year);

        $dias = Carbon::now()->diffInDays($boda->fecha);

        $response->assertSeeText($dias);
    }

    private function crearBoda()
    {
        $boda = new Boda();
        $boda->nombres = $this->faker->name('female') . ' & ' . $this->faker->name('male');
        $boda->fecha = $this->faker->date();

        $boda->save();

        return $boda;
    }
}
