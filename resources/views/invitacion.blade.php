<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $boda->nombres . ' - ¡Nos casamos!' }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/invitacion.css') }}" rel="stylesheet"  media="all">
</head>
<body class="min-h-full bg-white" style="background-image: url('img/corazones.jpg');">

  @if ($preguntar_asistencia)
    <div class="justify-center text-center">
        <div class="lg:p-4 p-2">
            <p class="text-xl">¿Contaremos con vuestra compañía?</p>

            <div class="lg:p-4 p-2">
                <form action="/invitacion" id="formulario-invitacion" method="post">
                    {{ csrf_field() }}
                    <button type="submit" name="asistencia" value="si" class="bg-green-500 hover:bg-green-700 text-white font-bold py-4 px-6 rounded-full">
                        Sí
                    </button>
                    <button type="submit" name="asistencia" value="no" class="bg-red-500 hover:bg-red-700 text-white font-bold py-4 px-6 rounded-full">
                        No
                    </button>
                </form>
            </div>
        </div>
    </div>
  @else
    <div class="justify-right text-right">
      <a href="/" class="enlace">Volver</a>
    </div>
  @endif

  @if ($solo_imagenes ?? false )
    <div class="flex flex-wrap items-center justify-center text-center">
      <div class="justify-center text-center lg:w-2/3 bg-white shadow-md rounded lg:p-8 p-2">
        <a href="/invitacion" class="enlace">Ver version tarjeta</a>
        <img class="mx-auto" src="../img/Invitacion1.png">
        <img class="mx-auto" src="../img/Invitacion2.png">
        <img class="mx-auto" src="../img/Invitacion3.png">
        <img class="mx-auto" src="../img/Invitacion4.png">
      </div>
    </div>
  @else
    <div class="justify-center text-center">
      <a href="/invitacion?solo_imagenes" class="enlace">Ver version solo imágenes</a>
    </div>
    <div class="justify-right text-right">
      <span class="text-gray-700 italic">Pulsa en la imágen para pasar de página</span>
    </div>
    <input type="checkbox" id="primero" class="check" >    
    <div class="tarjeta uno" >
      <div class="hojas">
        <label for="primero" class="hoja derecha portada">
          <img src="../img/Invitacion1.png">
        </label>
        <label for="primero" class="hoja izquierda interior1">
          <img src="../img/Invitacion2.png">
        </label>
      </div>
    </div>
    <div class="dos">
        <input type="checkbox" id="segundo" class="check" >    
        <div class="tarjeta" >
          <div class="hojas">
            <label for="segundo" class="hoja derecha interior2">
              <img src="../img/Invitacion3.png">
            </label>
            <label for="segundo" class="hoja izquierda contraportada">
              <img src="../img/Invitacion4.png">
            </label>
          </div>
        </div>
    </div>
  @endif





</body>
</html>