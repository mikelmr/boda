@extends('layouts.app')
@section('content')

<table class="border-collapse w-full">
    <thead>
        <tr>
            <th class="bg-blue-100 border text-center hidden lg:table-cell">Nombre</th>
            <th class="bg-blue-100 border text-center hidden lg:table-cell">Login</th>
            <th class="bg-blue-100 border text-center hidden lg:table-cell">Asistirá</th>
            <th class="bg-blue-100 border text-center hidden lg:table-cell">Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($usuarios as $usuario)
        <tr class="bg-white lg:hover:bg-gray-100 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0">
            <td class="w-full lg:w-auto p-3 text-gray-800 text-center border border-b block lg:table-cell relative lg:static">
                <span class="lg:hidden absolute top-0 left-0 h-full w-24 bg-blue-100 px-2 py-1 text-xs font-bold uppercase">Nombre</span>
                {{ $usuario->name }}
            </td>
            <td class="w-full lg:w-auto p-3 text-gray-800 text-center border border-b block lg:table-cell relative lg:static">
                <span class="lg:hidden absolute top-0 left-0 h-full w-24 bg-blue-100 px-2 py-1 text-xs font-bold uppercase">Login</span>
                {{ $usuario->usuario }}
            </td>
            <td class="w-full lg:w-auto p-3 text-gray-800 text-center border border-b block lg:table-cell relative lg:static">
                <span class="lg:hidden absolute top-0 left-0 h-full w-24 bg-blue-100 px-2 py-1 text-xs font-bold uppercase">Asistirá</span>
                {{ $usuario->asistencia_confirmada === null ? 'Sin Responder' : ($usuario->asistencia_confirmada ? 'Sí' : 'No') }}
            </td>
            <td class="w-full lg:w-auto p-3 text-gray-800 text-center border border-b block lg:table-cell relative lg:static">
                <span class="lg:hidden absolute top-0 left-0 h-full w-24 bg-blue-100 px-2 py-1 text-xs font-bold uppercase">Acciones</span>
                <a href="{{ URL::to('administrar/cuestionario/'. $usuario->id) }}" title="Cuestionario"
                    class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-2 mx-2 rounded inline-flex items-center text-sm">
                    <svg viewBox="0 0 24 24" stroke="black" class="fill-current w-4 h-4">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12h6m-6 4h6m2 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z"></path>
                    </svg>
                </a>
                @if ($usuario->token)
                    <input id="txt_{{ $usuario->id }}" type="text" readonly="readonly" class="w-0" value="{{ $usuario->obtenerUrlReseteo() }}">
                    <a href="#" onclick="copiar('txt_{{ $usuario->id }}');" title="Copiar al Portapapeles"
                        class="bg-gray-500 hover:bg-grat-700 text-white font-bold py-1 px-2 mx-2 rounded inline-flex items-center text-sm">
                        <svg viewBox="0 0 24 24" stroke="black" 
                            class="fill-current w-4 h-4"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2"></path></svg>
                    </a>
                    <a href="whatsapp://send?text=Me gustaría invitarte a mi boda :-)%0A{{$usuario->obtenerUrlReseteo()}}" title="Whatsapp"
                        class="bg-green-400 hover:bg-green-700 text-white font-bold py-1 px-2 mx-2 rounded inline-flex items-center text-sm">
                      <svg class="fill-current w-4 h-4" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                            viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                        <path style="fill:#4CAF50;" d="M256.064,0h-0.128l0,0C114.784,0,0,114.816,0,256c0,56,18.048,107.904,48.736,150.048l-31.904,95.104
                            l98.4-31.456C155.712,496.512,204,512,256.064,512C397.216,512,512,397.152,512,256S397.216,0,256.064,0z"/>
                        <path style="fill:#FAFAFA;" d="M405.024,361.504c-6.176,17.44-30.688,31.904-50.24,36.128c-13.376,2.848-30.848,5.12-89.664-19.264
                            C189.888,347.2,141.44,270.752,137.664,265.792c-3.616-4.96-30.4-40.48-30.4-77.216s18.656-54.624,26.176-62.304
                            c6.176-6.304,16.384-9.184,26.176-9.184c3.168,0,6.016,0.16,8.576,0.288c7.52,0.32,11.296,0.768,16.256,12.64
                            c6.176,14.88,21.216,51.616,23.008,55.392c1.824,3.776,3.648,8.896,1.088,13.856c-2.4,5.12-4.512,7.392-8.288,11.744
                            c-3.776,4.352-7.36,7.68-11.136,12.352c-3.456,4.064-7.36,8.416-3.008,15.936c4.352,7.36,19.392,31.904,41.536,51.616
                            c28.576,25.44,51.744,33.568,60.032,37.024c6.176,2.56,13.536,1.952,18.048-2.848c5.728-6.176,12.8-16.416,20-26.496
                            c5.12-7.232,11.584-8.128,18.368-5.568c6.912,2.4,43.488,20.48,51.008,24.224c7.52,3.776,12.48,5.568,14.304,8.736
                            C411.2,329.152,411.2,344.032,405.024,361.504z"/>
                        </svg>
                    </a>
                @else
                    <a href="{{ URL::to('administrar/usuarios/' . $usuario->id . '/reset') }}" title="Regenerar URL"
                        class="bg-yellow-500 hover:bg-yellow-700 text-yellow-500 hover:text-yellow-700 font-bold py-1 px-2 mx-2 rounded inline-flex items-center text-sm">
                        <svg viewBox="0 0 24 24" stroke="black" class="fill-current w-4 h-4">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 4v5h.582m15.356 2A8.001 8.001 0 004.582 9m0 0H9m11 11v-5h-.581m0 0a8.003 8.003 0 01-15.357-2m15.357 2H15" />
                        </svg>
                    </a>
                @endif
            </td>
        </tr>
        @endforeach        
    </tbody>
</table>

    <div class="p-2">
        <a href="{{ URL::to('administrar/usuarios/reset') }}" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Resetear Todos</a>
    </div>
@endsection

@push('scripts')
    <script language="javascript">
        function copiar(id) {
            /* Get the text field */
            var copyText = document.getElementById(id);

            /* Select the text field */
            copyText.select();
            copyText.setSelectionRange(0, 99999); /* For mobile devices */

            /* Copy the text inside the text field */
            document.execCommand("copy");
        } 
    </script>
@endpush