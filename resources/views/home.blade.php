@extends('layouts.app')
@section('content')
    <div class="w-full text-center">
        @if(Auth::user() && Auth::user()->asistencia_confirmada)
            <h2 class="text-2xl">Te esperamos!</h2>
        @endif
    </div>
    <img src="{{ $boda->obtenerImagenPortada() }}" />
@endsection
