@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <h2 class="etiqueta-pregunta" for="grid-first-name">
                    Inicio de Sesión
                </h2>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div id="formulario-registro-errores" style="color:red; font-weight: bold">
                                @if($errors->any())
                                    <ul>
                                        @foreach($errors->all() as $cadaError)
                                            <li>{{ $cadaError }}</li>
                                        @endforeach
                                    </ul>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="w-full">                            
                            <div class="flex flex-wrap -mx-3 mb-2">
                                <div class="row w-full md:flex md:items-center m-3">
                                    <div class="md:w-1/3">
                                        <label class="etiqueta" for="usuario">
                                            Usuario:
                                        </label>
                                    </div>
                                    <div class="md:w-2/3">
                                        <input id="usuario" name="usuario" type="text" 
                                            placeholder="Usuario" value="{{ old('usuario') }}"
                                            class="control form-control" 
                                            autocomplete="usuario" required autofocus>
                                    </div>
                                </div>
                                <div class="row w-full md:flex md:items-center m-3">
                                    <div class="md:w-1/3">
                                        <label class="etiqueta" for="password">
                                            Contraseña:
                                        </label>
                                    </div>
                                    <div class="md:w-2/3">
                                        <input id="password" name="password" type="password" 
                                            placeholder="Contraseña" value="{{ old('password') }}"
                                            class="control form-control"
                                            required autocomplete="current-password">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="bg-green-500 hover:bg-green-700 text-white font-bold py-4 px-6 rounded-full">
                                    Entrar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection