@extends('layouts.app')
@section('content')
    <div class="w-full text-center">
        @foreach ($eventos as $evento)
            <div>
                <h2 class="text-xl font-semibold">{{ $evento->hora->isoFormat('HH:mm') . ' - ' . $evento->nombre }}</h2>
                @if (!empty($evento->texto))
                    <p>{{ $evento->texto }}</p>
                @endif
                @if ($evento->link_lugar)
                    <iframe width="100%" height="350" src="{!! $evento->link_lugar !!}" ></iframe>
                @endif
                @if ($evento->latitud && $evento->longitud)
                <small><a class="enlace" href="https://www.openstreetmap.org/?mlat={{ $evento->latitud }}&amp;mlon={{ $evento->longitud }}#map=19/{{ $evento->latitud }}/{{ $evento->longitud }}&amp;layers=N">Ver mapa más grande</a></small>
                <a class="enlace" href="geo:{{ $evento->latitud }},{{ $evento->longitud }}">Como llegar</a>
                @endif
            </div>
        @endforeach
    </div>
@endsection
