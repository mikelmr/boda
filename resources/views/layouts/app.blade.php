<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $boda->nombres . ' - ¡Nos casamos!' }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @stack('styles')
</head>
<body class="min-h-full" style="background-image: url('img/corazones.jpg');">
    @include('layouts.partials.header')

    @include('layouts.partials.nav')

    <div class="flex-center position-ref full-height">
        <div class="flex flex-wrap items-center justify-center text-center" >
            <div class="lg:w-2/3 xl:w-1/2 bg-white bg-opacity-75 shadow-md rounded lg:p-8 p-2">
                @yield('content')
            </div>
        </div>
    </div>

    @stack('scripts')
</body>
</html>
