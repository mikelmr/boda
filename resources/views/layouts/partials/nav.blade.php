@if(Auth::user() && Auth::user()->asistencia_confirmada)
    <input id="hamburger" type="checkbox" class="hamburger hidden">
    <label for="hamburger" class="block md:hidden">
        <span class="mostrar hidden"><svg width="24" height="24" fill="none" viewBox="0 0 24 24" stroke="currentColor" class="inline transform transition-transform duration-500 ease-in-out"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"></path><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"></path></svg>Mostrar menú</span>
        <span class="ocultar"><svg width="24" height="24" fill="none" viewBox="0 0 24 24" stroke="currentColor" class="inline transform transition-transform duration-500 ease-in-out"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13.875 18.825A10.05 10.05 0 0112 19c-4.478 0-8.268-2.943-9.543-7a9.97 9.97 0 011.563-3.029m5.858.908a3 3 0 114.243 4.243M9.878 9.878l4.242 4.242M9.88 9.88l-3.29-3.29m7.532 7.532l3.29 3.29M3 3l3.59 3.59m0 0A9.953 9.953 0 0112 5c4.478 0 8.268 2.943 9.543 7a10.025 10.025 0 01-4.132 5.411m0 0L21 21"></path></svg>Ocultar menú</span>
    </label>    
    <nav class="menu bg-white border">
        <ul class="flex-col flex-grow pb-2 p-2 xs:pb-0 md:flex md:justify-end md:flex-row">
            @if(Auth::user()->tienePermiso('Administrador'))
                <li class="mr-6">
                    <a class="border md:border-0 block px-4 py-1 mt-2 text-sm font-semibold bg-transparent rounded-lg md:mt-0 md:ml-4 hover:text-gray-900 focus:text-gray-900 hover:bg-gray-200 focus:bg-gray-200 focus:outline-none focus:shadow-outline" href="/administrar/usuarios">Administrar Usuarios</a>
                </li>
            @endif
            <li class="mr-6">
                <a class="border md:border-0 block px-4 py-1 mt-2 text-sm font-semibold bg-transparent rounded-lg md:mt-0 md:ml-4 hover:text-gray-900 focus:text-gray-900 hover:bg-gray-200 focus:bg-gray-200 focus:outline-none focus:shadow-outline" href="/informacion">Información</a>
            </li>
            <li class="mr-6">
                <a class="border md:border-0 block px-4 py-1 mt-2 text-sm font-semibold bg-transparent rounded-lg md:mt-0 md:ml-4 hover:text-gray-900 focus:text-gray-900 hover:bg-gray-200 focus:bg-gray-200 focus:outline-none focus:shadow-outline" href="/invitacion">Invitación</a>
            </li>
            <li class="mr-6">
                <a class="border md:border-0 block px-4 py-1 mt-2 text-sm font-semibold bg-transparent rounded-lg md:mt-0 md:ml-4 hover:text-gray-900 focus:text-gray-900 hover:bg-gray-200 focus:bg-gray-200 focus:outline-none focus:shadow-outline" href="/cuestionario">Cuestionario</a>
            </li>
            <li class="mr-6">
                <a class="border md:border-0 block px-4 py-1 mt-2 text-sm font-semibold bg-transparent rounded-lg md:mt-0 md:ml-4 hover:text-gray-900 focus:text-gray-900 hover:bg-gray-200 focus:bg-gray-200 focus:outline-none focus:shadow-outline" href="/album">Fotos</a>
            </li>
            @if($boda->haLlegadoFecha() || Auth::user()->tienePermiso('Administrador'))
                <li class="mr-6">
                    <a class="border md:border-0 block px-4 py-1 mt-2 text-sm font-semibold bg-transparent rounded-lg md:mt-0 md:ml-4 hover:text-gray-900 focus:text-gray-900 hover:bg-gray-200 focus:bg-gray-200 focus:outline-none focus:shadow-outline" href="/mis-fotos">Mis-Fotos</a>
                </li>
            @endif
        </ul>
    </nav>
@endif