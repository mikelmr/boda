<header class="flex flex-wrap cabecera" style="background-image:url('{{ $boda->obtenerImagenCabecera() }}');">
    <div class="md:w-2/5 ml-auto mr-auto bg-gray-100 bg-opacity-50 m-2 p-4 rounded-lg shadow-2xl">
        <div class="text-xl">
            ¡Nos casamos!
        </div>
        <div class="text-gray-700 content-center text-center md:text-6xl text-3xl px-4 py-2 m-2">
            {{ $boda->nombres }}
        </div>
        <div class="text-xl">
            {{ $boda->obtenerFechaTexto() }}
        </div>
    </div>
    <div class="md:w-1/5 items-center">
        <ul class="text-right">
        @guest
            <li class="nav-item">
                <a class="nav-link enlace" href="{{ route('login') }}">Entrar</a>
            </li>
        @else
            <li class="nav-item dropdown">
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item enlace" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                        Salir
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </div>
            </li>
        @endguest
        </ul>
        <div class="rounded-full flex flex-col bg-white text-center h-24 w-24 p-2 m-auto shadow-2xl">
            faltan
            <h3 class="text-2xl">{{ $boda->obtenerDiasFaltan() }}</h3>
            días
        </div>
        <!-- Right Side Of Navbar -->

    </div>
</header>