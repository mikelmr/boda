@extends('layouts.app')
@section('content')

    @if($anterior || $siguiente)
        <div class="w-full">
            @if($anterior)
                <a href="/album/{{$anterior->nombre}}" class="enlace float-left" >Anterior</a>
            @endif    
            @if($siguiente)
                <a href="/album/{{$siguiente->nombre}}" class="enlace float-right" >Siguiente</a>
            @endif
        </div>
    @endif
    <div class="rounded shadow-lg bg-white border border-gray-500">
        <img class="mb-1 object-contain w-full h-96" src="{{ $foto->ruta() }}" loading="lazy" />
        <div class="flex justify-center mt-1 relative h-5">
            <a href="{{$foto->ruta()}}" class="w-full bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-6 absolute inset-x-0 bottom-0" download>Descargar</a>
        </div>
    </div>
    <a href="/album" class="enlace">Volver</a>
@endsection
