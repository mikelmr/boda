@extends('layouts.app')
@section('content')
    @if($fotos->previousPageUrl() || $fotos->nextPageUrl())
        <div class="w-full">
            @if($fotos->previousPageUrl())
                <a href="{{$fotos->previousPageUrl()}}" class="enlace float-left" >Anterior</a>
            @endif
            @if($fotos->nextPageUrl())
                <a href="{{$fotos->nextPageUrl()}}" class="enlace float-right" >Siguiente</a>
            @endif
        </div>
    @endif
    <div class="grid grid-flow-row-dense grid-cols-2 gap-3 justify-between sm:grid-cols-3 md:grid-cols-4">
        @foreach ($fotos as $foto)
            <div class="rounded shadow-lg bg-white border border-gray-500 overflow-hidden h-40 flex flex-col justify-between">
                <img class="mb-1 object-contain w-full h-32" src="{{ $foto->ruta() }}" loading="lazy" />
                <div class="flex justify-center mt-1 relative h-5">
                    <a href="album/{{$foto->nombre}}" class="w-full bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-6 absolute inset-x-0 bottom-0" >Ver</a>
                </div>
            </div>
        @endforeach
    </div>
@endsection
