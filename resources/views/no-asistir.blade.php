@extends('layouts.app')
@section('content')
    <div class="flex-center position-ref full-height">
        <div class="flex flex-wrap items-center justify-center">
            <div class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
                <div class="w-full text-center">
                    <div>
                        <p>
                            Sentimos mucho que no vengas
                        </p>

                        <img src='/imagenes/triste.png' />
                        
                        <p>
                            Si se trata de un error ponte en contacto con nosotros.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
