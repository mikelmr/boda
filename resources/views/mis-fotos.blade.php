@extends('layouts.app')
@section('content')

    <form action="" method="post" enctype="multipart/form-data">
        @csrf
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <strong>{{ $message }}</strong>
            </div>
        @endif

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="flex w-full mb-4 items-center justify-center bg-grey-lighter">
            <div class="rounded shadow-lg overflow-hidden h-40 flex flex-col justify-between">
                <label class="w-128 h-4/5  flex flex-col items-center px-4 py-6 bg-white text-blue rounded-lg shadow-lg tracking-wide uppercase border border-blue cursor-pointer hover:bg-blue-500">
                    <svg class="w-full h-8" fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                        <path d="M16.88 9.1A4 4 0 0 1 16 17H5a5 5 0 0 1-1-9.9V7a3 3 0 0 1 4.52-2.59A4.98 4.98 0 0 1 17 8c0 .38-.04.74-.12 1.1zM11 11h3l-4-4-4 4h3v3h2v-3z" />
                    </svg>
                    <span class="mt-2 text-base leading-normal">Sube fotos</span>
                    <input type='file' multiple="multiple" name="fotos[]" />
                </label>
                <div class="flex justify-center mt-1 relative h-8">
                    <button type="submit" name="submit" class="w-full bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-6 absolute inset-x-0 bottom-0">
                        Subir
                    </button>
                </div>
            </div>
        </div>
    </form>

    <div class="grid grid-flow-row-dense grid-cols-2 gap-3 justify-between sm:grid-cols-3 md:grid-cols-4">
        @foreach ($fotos as $foto)
            <form action="mis-fotos/{{$foto->nombre}}" method="post" enctype="multipart/form-data">
                @method('delete')
                @csrf

                <div class="rounded shadow-lg bg-white border border-gray-500 overflow-hidden h-40 flex flex-col justify-between">
                    <img class="mb-1 object-contain w-full h-32" src="{{ $foto->ruta() }}" loading="lazy" />
                    <div class="flex justify-center mt-1 relative h-5">
                        <input class="w-full bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-6 absolute inset-x-0 bottom-0" type="submit" value="Eliminar" />
                    </div>
                </div>
            </form>
        @endforeach
    </div>
@endsection
