@extends('layouts.app')
@section('content')
    <h1 class="text-xl uppercase p-2">Cuestionario</h1>

    <form action="/cuestionario" id="formulario-cuestionario" method="post">
        {{ csrf_field() }}
        @if(isset($idUsuario) && !empty($idUsuario))
            <input type="hidden" name="idUsuario" value="{{$idUsuario}}" />
        @endif

        <div class="p-2 w-full flex flex-wrap justify-content-center items-center">
            
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <div id="formulario-registro-errores" style="color:red; font-weight: bold">
                            @if($errors->any())
                                <ul>
                                    @foreach($errors->all() as $cadaError)
                                        <li>{{ $cadaError }}</li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="w-full">
                <label class="etiqueta-pregunta" for="grid-first-name">
                    ¿Cuantos venís?
                </label>
                <div class="flex flex-wrap -mx-3 mb-2">
                    <div class="w-full md:w-1/2 flex items-center">
                        <div class="w-2/3">
                            <label class="etiqueta" for="adultos">
                                Adultos:
                            </label>
                        </div>
                        <div class="w-16">
                            <input id="adultos" name="adultos" required type="text" 
                                value="{{ old('adultos', $cuestionario->adultos) }}"
                                class="control text-center @error('adultos') border-red-600 @enderror">
                        </div>
                        
                    </div>
                    <div class="w-full md:w-1/2 flex items-center">
                        <div class="w-2/3">
                            <label class="etiqueta" for="menores">
                                Menores:
                            </label>
                        </div>
                        <div class="w-16">
                            <input id="menores" name="menores" type="text"
                                value="{{ old('menores', $cuestionario->menores) }}"
                                class="control text-center">
                        </div>
                    </div>
                    <div class="w-full md:w-1/2 flex items-center">
                        <div class="w-2/3">
                            <label class="etiqueta" for="veganos">
                                Veganos:
                            </label>
                        </div>
                        <div class="w-16">
                            <input id="veganos" name="veganos" type="text"
                                value="{{ old('veganos', $cuestionario->veganos) }}"
                                class="control text-center">
                        </div>
                    </div>
                </div>
            </div>

            <div class="w-full">
                <label class="etiqueta text-gray-700" for="intolerancia">
                    ¿Alguna intolerancia alimenticia?
                </label>
                <div class="w-full">
                    <input id="intolerancia" name="intolerancia" type="text" 
                        placeholder="Intolerancias.." value="{{ old('intolerancia', $cuestionario->intolerancia) }}"
                        class="control">
                </div>
            </div>

            <div class="w-full md:w-1/2 px-3">
                <label class="etiqueta-pregunta" for="autobus">
                    ¿Cómo acudirás?
                </label>
                <div class="relative">
                    <select id="autobus" name="autobus" class="control" >
                        <option value="autobus" @if(old('autobus', $cuestionario->autobus) == "autobus") selected @endif>Autobús</option>
                        <option value="coche" @if(old('autobus', $cuestionario->autobus) == "coche") selected @endif>Coche</option>
                    </select>
                    <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                        <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                    </div>
                </div>
            </div>
            <div class="w-full md:w-1/2 px-3">
                <label class="etiqueta-pregunta" for="hora">
                    ¿En qué autobús te irías?
                </label>
                <div class="relative">
                    <select class="control" id="hora" name="hora">
                    <option value="">...</option>
                    <option value="medianoche" @if(old('hora', $cuestionario->hora) == "medianoche") selected @endif>Medianoche</option>
                    <option value="madrugada" @if(old('hora', $cuestionario->hora) == "madrugada") selected @endif>Madrugada</option>
                    </select>
                    <div class="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                        <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                    </div>
                </div>
            </div>

            <div class="w-full">
                <label class="etiqueta-pregunta" for="grid-first-name">
                    ¿Tienes mascota?
                </label>
                <div class="flex flex-wrap -mx-3 mb-2">
                    <div class="w-full md:w-1/2 md:items-center mb-6">
                        <div class="block w-full">
                            <label class="etiqueta" for="mascota_perro">
                                Nombre Perros:
                            </label>
                        </div>
                        <div class="block w-full">
                            <textarea id="mascota_perro" name="mascota_perro" type="textarea" rows="3"
                                placeholder="Nombre de los perros" class="control w-3/4" >{{ old('mascota_perro', $cuestionario->mascota_perro) }}</textarea>
                        </div>
                    </div>
                    <div class="w-full md:w-1/2 md:items-center mb-6">
                        <div class="block">
                            <label class="etiqueta" for="mascota_gato">
                                Nombre Gatos:
                            </label>
                        </div>
                        <div class="block">
                            <textarea  id="mascota_gato" name="mascota_gato" type="text" rows="3"
                                placeholder="Nombre de los Gatos" class="control w-3/4" >{{ old('mascota_gato', $cuestionario->mascota_gato) }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="w-full">
                <button type="submit" name="guardar" class="bg-green-500 hover:bg-green-700 text-white font-bold py-4 px-6 rounded-full">
                    Guardar
                </button>
            </div>
        </div>
    </form>
@endsection
